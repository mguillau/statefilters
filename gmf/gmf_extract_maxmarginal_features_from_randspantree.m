function [features,GM,info] = gmf_extract_maxmarginal_features_from_randspantree(GM,G,EG,pw_fun,pw_fun_args)

% sample a new random spanning tree from the graph
tree = graph_randspantree(G);
% add the tree structure to the GM
GM = tgm_from_egraph_gm_tree(GM,EG,tree);
% compute needed pairwise terms
[GM,info] = tgmf_compute_tree_pairwise(GM,pw_fun,pw_fun_args);
% compute min-marginals on tree with BP
M = tgmf_minmarginals_bp(GM);
% remove tree from GM
GM = rmfield(GM,'tree');
% transform minmarginals into a feature vector
features = features_from_minmarginals(M);

