function [GM,info] = gmf_compute_remaining_pairwise(GM,pw_fun,pw_fun_args)

npairwise = 0;
unary_sizes = cellsizes(GM.UE);
unary_sizes = unary_sizes(:,2);
state_sizes = cellsizes(GM.filter.NI);
state_sizes = state_sizes(:,2);
empty_energy = cellisempty(GM.PE); %any(cellsizes(GM.PE),2);
total_time = tic();

if size(GM.PI,1)==2,
    K = 1:size(GM.PI,2);
else
    K = GM.PI(3,:);
end

for e=1:size(GM.PI,2)
    i = GM.PI(1,e);
    j = GM.PI(2,e);
    k = K(e);
    if empty_energy(k),
        E = Inf( unary_sizes(i), unary_sizes(j) );
        E( GM.filter.NI{i}, GM.filter.NI{j} ) = feval( pw_fun, i, GM.filter.NI{i}, j, GM.filter.NI{j}, pw_fun_args{:} );
        GM.PE{k} = E;
        npairwise = npairwise + state_sizes(i)*state_sizes(j);
    end
end

info.time = toc(total_time);
info.new_pairwise = npairwise; 

