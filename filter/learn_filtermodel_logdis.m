function [filtermodel,info] = learn_filtermodel_logdis(GMs,varargin)

read_varargin;
default numberOfTrees 1; % number of trees per GM and per iteration
default unaryAtFirstIter true; % should the first iteration operate on unaries alone?
default filterLimit 1; % always keep at least this number of states at each node
default maxNumberIterations 20; % maximum number of iterations
%default accumulateMaxMarginals false; % 
default splitMaxMarginals false; 
default targetSolveArgs {'method','trws'}; % optional arguments for gm_solve

N = length(GMs); % number of training GMs
GMs = reshape(GMs,1,N);
Gs = cellfun2(@graph_from_gm,GMs); % get the graphs of the GMs
EGs = cellfun2(@egraph_from_gm,GMs); % efficient indexing of edges

maxs = max(cellfun(@gm_maxstates,GMs)); % max number of states in any GM
nt = cellfun(@gm_number_pairwise,GMs);
targetSolution = cell(1,N);    % TRWS solution
targetEnergy = zeros(1,N);     % and energy

pg_message('Computing target solutions for the GMs');
for i=1:N,
     pg_message('GM %d (%d nodes, %d pw)',i,length(GMs{i}.UE),nt(i));
     [targetEnergy(i),targetSolution{i},targetInfo(i)]=gm_solve(GMs{i},targetSolveArgs{:});
end 

if length(filterLimit)<maxNumberIterations,
    filterLimit(end+1:maxNumberIterations) = filterLimit(end);
end

if splitMaxMarginals,
    mm_feat_fun = @gmf_extract_split_minmarginal_features_from_randspantree;
else
    mm_feat_fun = @gmf_extract_minmarginal_features_from_randspantree;
end
    
filtermodel = cell(1,maxNumberIterations);
npw = zeros(maxNumberIterations,N);
NI = cell(maxNumberIterations+1,N); % keep track of filtering
info.iter = cell(1,maxNumberIterations);

pg_message('Initializing filters');
for n=1:N,
    GMs{n} = gm_init_filter(GMs{n});
    NI{1,n} = GMs{n}.filter.NI;
end

for k=1:maxNumberIterations,
    
    pg_begin(sprintf('*** ITER %d STARTING ***',k));
    filtermodel{k}.limit = filterLimit(k);
    filtermodel{k}.classify = @filter_logdis;

    pg_message('Extracting features for learning');
    if k==1 && unaryAtFirstIter,
        filtermodel{k}.feature_fun = @gmf_extract_unary_features;
        features = extract_unary_features_for_learning(GMs,Gs,EGs);
    else
        filtermodel{k}.feature_fun = mm_feat_fun;
        features = extract_features_for_learning(GMs,Gs,EGs,mm_feat_fun);
    end
    
    pg_message('Learning filter');
    filtermodel{k}.model = learn_filter_from_features(GMs,features,targetSolution);

    pg_message('Applying filter on training GMs');
    for n=1:N,
        [GMs{n},info.iter{k}] = gmf_apply_filter(filtermodel{k},GMs{n},Gs{n},EGs{n});
        NI{k+1,n} = GMs{n}.filter.NI;
        npw(k,n) = gmf_number_pairwise(GMs{n});
        numberLostSolutions(n,k+1) = nnz(~cellfun(@ismember,num2cell(targetSolution{n}),NI{k+1,n}));
    end
    pg_message('After iter %d, the number of remaining pw terms is: %2.3f%%',k,mean(100*npw(k,:)./nt));

    pg_message('Checking convergence and lost solutions');
    if any(numberLostSolutions(:,k+1)>numberLostSolutions(:,k)),
        pg_message('%d more solutions have been lost',sum(numberLostSolutions(:,k+1)-numberLostSolutions(:,k)));
    end
    converged = cellfun(@isequaln,NI(k+1,:),NI(k,:))
    if converged,
        pg_message('Converged: stopping before maxIter is reached');
        pg_end();
        break;
    end
    pg_end();

end

if k<maxNumberIterations,
    filtermodel = filtermodel(1:k);
    NI = NI(1:k+1,:);
    numberLostSolutions(n,1:k+1);
    npw = npw(1:k,n);
    info.iter = info.iter(1:k);
end
info.full_pw = nt;
info.number_lost_solutions = numberLostSolutions(:,end);
info.progressive_pw_reduciton = npw;

