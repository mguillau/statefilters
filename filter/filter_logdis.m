function [score,idx] = filter_logdis(filter,X)

score = -full(X' * filter.w);
score = softmax(score,2);
score = score(:,1);
idx   = score >= filter.p;
