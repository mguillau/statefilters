function [filtermodel,info] = learn_filtermodel_dummy(n_iter,frac,limit)

filtermodel = cell(1,n_iter);

for i=1:n_iter,
    filtermodel{i}.feature_fun = @gmf_extract_minmarginal_features_from_randspantree; % just use max-marginals directly
    filtermodel{i}.limit = limit;
    filtermodel{i}.frac = frac;
    filtermodel{i}.classify = @filter_top; % just best top-frac fraction of min-marginals
end

info.n_iter = n_iter;

