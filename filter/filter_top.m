function [scores,idx] = filter_top(filter,X)

scores = -X;
[~,ii] = top(scores,filter.frac);
idx = false(1,length(X));
idx(ii) = true;

