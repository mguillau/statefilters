/******************************************************************
TypeTwostate.h

Energy function with Potts interactions:
   E(x)   =   \sum_i D_i(x_i)   +   \sum_ij V_ij(x_i,x_j)
   where x_i \in {0, 1, ..., K-1},
   V_ij(ki, kj) = 0 if ki==kj, and lambda_ij otherwise.
   lambda_ij must be non-negative.

Example usage:

Minimize function E(x,y) = Dx(x) + Dy(y) + lambda*[x != y] where 
  x,y \in {0,1,2},
  Dx(0) = 0, Dx(1) = 1, Dx(2) = 2,
  Dy(0) = 3, Dy(1) = 4, Dy(2) = 5,
  lambda = 6,
  [.] is 1 if it's argument is true, and 0 otherwise.



#include <stdio.h>
#include "MRFEnergy.h"

void testPotts()
{
	MRFEnergy<TypeTwostate>* mrf;
	MRFEnergy<TypeTwostate>::NodeId* nodes;
	MRFEnergy<TypeTwostate>::Options options;
	TypeTwostate::REAL energy, lowerBound;

	const int nodeNum = 2; // number of nodes
	const int K = 3; // number of labels
	TypeTwostate::REAL D[K];
	int x, y;

	mrf = new MRFEnergy<TypeTwostate>(TypeTwostate::GlobalSize(K));
	nodes = new MRFEnergy<TypeTwostate>::NodeId[nodeNum];

	// construct energy
	D[0] = 0; D[1] = 1; D[2] = 2;
	nodes[0] = mrf->AddNode(TypeTwostate::LocalSize(), TypeTwostate::NodeData(D));
	D[0] = 3; D[1] = 4; D[2] = 5;
	nodes[1] = mrf->AddNode(TypeTwostate::LocalSize(), TypeTwostate::NodeData(D));
	mrf->AddEdge(nodes[0], nodes[1], TypeTwostate::EdgeData(6));

	// Function below is optional - it may help if, for example, nodes are added in a random order
	// mrf->SetAutomaticOrdering();

	/////////////////////// TRW-S algorithm //////////////////////
	options.m_iterMax = 30; // maximum number of iterations
	mrf->Minimize_TRW_S(options, lowerBound, energy);

	// read solution
	x = mrf->GetSolution(nodes[0]);
	y = mrf->GetSolution(nodes[1]);

	printf("Solution: %d %d\n", x, y);

	//////////////////////// BP algorithm ////////////////////////
	mrf->ZeroMessages(); // in general not necessary - it may be faster to start 
	                     // with messages computed in previous iterations

	options.m_iterMax = 30; // maximum number of iterations
	mrf->Minimize_BP(options, energy);

	// read solution
	x = mrf->GetSolution(nodes[0]);
	y = mrf->GetSolution(nodes[1]);

	printf("Solution: %d %d\n", x, y);

	// done
	delete nodes;
	delete mrf;
}

*******************************************************************/


















#ifndef __TYPETWOSTATE_H__
#define __TYPETWOSTATE_H__

#include <string.h>
#include <assert.h>


template <class T> class MRFEnergy;


class TypeTwostate
{

public:
	// types declarations
	struct Edge; // stores edge information and either forward or backward message
	struct Vector; // node parameters and messages
	typedef int Label;
	typedef double REAL;
	struct GlobalSize; // global information about number of labels
	struct LocalSize; // local information about number of labels (stored at each node)
	struct NodeData; // argument to MRFEnergy::AddNode()
	struct EdgeData; // argument to MRFEnergy::AddEdge()


	struct GlobalSize
	{
		GlobalSize(int K);

	private:
	friend struct Vector;
	friend struct Edge;
		int		m_K; // number of labels
	};

	struct LocalSize // number of labels is stored at MRFEnergy::m_Kglobal
	{
  LocalSize(int K);

	private:
	friend struct Vector;
	friend struct Edge;
		int		m_K; // number of labels
	};

	struct NodeData
	{
		NodeData(REAL* data); // data = pointer to array of size MRFEnergy::m_Kglobal

	private:
	friend struct Vector;
	friend struct Edge;
		REAL*		m_data;
	};

	struct EdgeData
	{
		//EdgeData(REAL lambdaPotts);
    EdgeData(REAL agreement, REAL disagreement);

	private:
	friend struct Vector;
	friend struct Edge;
		//REAL		m_lambdaPotts;
    REAL		m_agreement;
    REAL		m_disagreement;
	};





	//////////////////////////////////////////////////////////////////////////////////
	////////////////////////// Visible only to MRFEnergy /////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////


friend class MRFEnergy<TypeTwostate>;

	struct Vector
	{
		static int GetSizeInBytes(GlobalSize Kglobal, LocalSize K); // returns -1 if invalid K's
		void Initialize(GlobalSize Kglobal, LocalSize K, NodeData data);  // called once when user adds a node
		void Add(GlobalSize Kglobal, LocalSize K, NodeData data); // called once when user calls MRFEnergy::AddNodeData()

		void SetZero(GlobalSize Kglobal, LocalSize K);                            // set this[k] = 0
		void Copy(GlobalSize Kglobal, LocalSize K, Vector* V);                    // set this[k] = V[k]
		void Add(GlobalSize Kglobal, LocalSize K, Vector* V);                     // set this[k] = this[k] + V[k]
		REAL GetValue(GlobalSize Kglobal, LocalSize K, Label k);                  // return this[k]
		REAL ComputeMin(GlobalSize Kglobal, LocalSize K, Label& kMin);            // return vMin = min_k { this[k] }, set kMin
		REAL ComputeAndSubtractMin(GlobalSize Kglobal, LocalSize K);              // same as previous, but additionally set this[k] -= vMin (and kMin is not returned)

	private:
	friend struct Edge;
		REAL		m_data[1]; // actual size is MRFEnergy::m_Kglobal
	};

	struct Edge
	{
		static int GetSizeInBytes(GlobalSize Kglobal, LocalSize Ki, LocalSize Kj, EdgeData data); // returns -1 if invalid data
		static int GetBufSizeInBytes(int vectorMaxSizeInBytes); // returns size of buffer need for UpdateMessage()
		void Initialize(GlobalSize Kglobal, LocalSize Ki, LocalSize Kj, EdgeData data, Vector* Di, Vector* Dj); // called once when user adds an edge
		Vector* GetMessagePtr();
		void Swap(GlobalSize Kglobal, LocalSize Ki, LocalSize Kj); // if the client calls this function, then the meaning of 'dir'
								                                               // in distance transform functions is swapped

		// When UpdateMessage() is called, edge contains message from dest to source.
		// The function must replace it with the message from source to dest.
		// The update rule is given below assuming that source corresponds to tail (i) and dest corresponds
		// to head (j) (which is the case if dir==0).
		//
		// 1. Compute Di[ki] = gamma*source[ki] - message[ki].  (Note: message = message from j to i).
		// 2. Compute distance transform: set
		//       message[kj] = min_{ki} (Di[ki] + V(ki,kj)). (Note: message = message from i to j).
		// 3. Compute vMin = min_{kj} m_message[kj].
		// 4. Set m_message[kj] -= vMin.
		// 5. Return vMin.
		//
		// If dir==1 then source corresponds to j, sink corresponds to i. Then the update rule is
		//
		// 1. Compute Dj[kj] = gamma*source[kj] - message[kj].  (Note: message = message from i to j).
		// 2. Compute distance transform: set
		//       message[ki] = min_{kj} (Dj[kj] + V(ki,kj)). (Note: message = message from j to i).
		// 3. Compute vMin = min_{ki} m_message[ki].
		// 4. Set m_message[ki] -= vMin.
		// 5. Return vMin.
		//
		// If Edge::Swap has been called odd number of times, then the meaning of dir is swapped.
		//
		// Vector 'source' must not be modified. Function may use 'buf' as a temporary storage.
		REAL UpdateMessage(GlobalSize Kglobal, LocalSize Ksource, LocalSize Kdest, Vector* source, REAL gamma, int dir, void* buf);

		// If dir==0, then sets dest[kj] += V(ksource,kj).
		// If dir==1, then sets dest[ki] += V(ki,ksource).
		// If Swap() has been called odd number of times, then the meaning of dir is swapped.
		void AddColumn(GlobalSize Kglobal, LocalSize Ksource, LocalSize Kdest, Label ksource, Vector* dest, int dir);

	private:
		// edge information
		//REAL		m_lambdaPotts;
    REAL		m_agreement;
    REAL		m_disagreement;
    int		m_dir; // 0 if Swap() was called even number of times, 1 otherwise
		// message
		Vector		*m_message;
	};
};




//////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// Implementation ///////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////


inline TypeTwostate::GlobalSize::GlobalSize(int K)
{
}

inline TypeTwostate::LocalSize::LocalSize(int K)
{
	m_K = K;
}

///////////////////// NodeData and EdgeData ///////////////////////

inline TypeTwostate::NodeData::NodeData(REAL* data)
{
	m_data = data;
}

// inline TypeTwostate::EdgeData::EdgeData(REAL lambdaPotts)
// {
// 	m_lambdaPotts = lambdaPotts;
// }
inline TypeTwostate::EdgeData::EdgeData(REAL agreement, REAL disagreement)
{
	m_agreement = agreement;
  m_disagreement = disagreement;
}

///////////////////// Vector ///////////////////////

inline int TypeTwostate::Vector::GetSizeInBytes(GlobalSize Kglobal, LocalSize K)
{
	if (K.m_K < 1)
	{
		return -1;
	}
	return K.m_K*sizeof(REAL);
}
inline void TypeTwostate::Vector::Initialize(GlobalSize Kglobal, LocalSize K, NodeData data)
{
	memcpy(m_data, data.m_data, K.m_K*sizeof(REAL));
}

inline void TypeTwostate::Vector::Add(GlobalSize Kglobal, LocalSize K, NodeData data)
{
	for (int k=0; k<K.m_K; k++)
	{
		m_data[k] += data.m_data[k];
	}
}

inline void TypeTwostate::Vector::SetZero(GlobalSize Kglobal, LocalSize K)
{
	memset(m_data, 0, K.m_K*sizeof(REAL));
}

inline void TypeTwostate::Vector::Copy(GlobalSize Kglobal, LocalSize K, Vector* V)
{
	memcpy(m_data, V->m_data, K.m_K*sizeof(REAL));
}

inline void TypeTwostate::Vector::Add(GlobalSize Kglobal, LocalSize K, Vector* V)
{
	for (int k=0; k<K.m_K; k++)
	{
		m_data[k] += V->m_data[k];
	}
}

inline TypeTwostate::REAL TypeTwostate::Vector::GetValue(GlobalSize Kglobal, LocalSize K, Label k)
{
	assert(k>=0 && k<K.m_K);
	return m_data[k];
}

inline TypeTwostate::REAL TypeTwostate::Vector::ComputeMin(GlobalSize Kglobal, LocalSize K, Label& kMin)
{
	REAL vMin = m_data[0];
	kMin = 0;
	for (int k=1; k<K.m_K; k++)
	{
		if (vMin > m_data[k])
		{
			vMin = m_data[k];
			kMin = k;
		}
	}

	return vMin;
}

inline TypeTwostate::REAL TypeTwostate::Vector::ComputeAndSubtractMin(GlobalSize Kglobal, LocalSize K)
{
	REAL vMin = m_data[0];
	for (int k=1; k<K.m_K; k++)
	{
		if (vMin > m_data[k])
		{
			vMin = m_data[k];
		}
	}
	for (int k=0; k<K.m_K; k++)
	{
		m_data[k] -= vMin;
	}

	return vMin;
}

///////////////////// EdgeDataAndMessage implementation /////////////////////////

inline int TypeTwostate::Edge::GetSizeInBytes(GlobalSize Kglobal, LocalSize Ki, LocalSize Kj, EdgeData data)
{
  int messageSizeInBytes = ((Ki.m_K > Kj.m_K) ? Ki.m_K : Kj.m_K)*sizeof(REAL);
  //return sizeof(Edge) - sizeof(REAL) + Ki.m_K*Kj.m_K*sizeof(REAL) + messageSizeInBytes
	return sizeof(Edge) + messageSizeInBytes;
}

inline int TypeTwostate::Edge::GetBufSizeInBytes(int vectorMaxSizeInBytes)
{
	return vectorMaxSizeInBytes;
}

inline void TypeTwostate::Edge::Initialize(GlobalSize Kglobal, LocalSize Ki, LocalSize Kj, EdgeData data, Vector* Di, Vector* Dj)
{
	m_agreement = data.m_agreement;
  m_disagreement = data.m_disagreement;
  m_dir = 0;
  m_message = (Vector*)((char*)this + sizeof(Edge));
}

inline TypeTwostate::Vector* TypeTwostate::Edge::GetMessagePtr()
{
	return m_message;
}

inline void TypeTwostate::Edge::Swap(GlobalSize Kglobal, LocalSize Ki, LocalSize Kj)
{
  		m_dir = 1 - m_dir;
}

inline TypeTwostate::REAL TypeTwostate::Edge::UpdateMessage(GlobalSize Kglobal, LocalSize Ksource, LocalSize Kdest, Vector* source, REAL gamma, int dir, void* _buf)
{
	Vector* buf = (Vector*) _buf;
	REAL vMin;


  
  int ksource, kdest;
  REAL* data = new REAL[Ksource.m_K * Kdest.m_K];
  for (int s=0; s <  Ksource.m_K ; s++)
  {
    for (int d=0 ; d < Kdest.m_K ; d++)
    {
      if (s == d)
        data[s+d*Ksource.m_K] = m_agreement;
      else
        data[s+d*Ksource.m_K] = m_disagreement;
    }
  }
  
  for (ksource=0; ksource<Ksource.m_K; ksource++)
  {
    buf->m_data[ksource] = gamma*source->m_data[ksource] - m_message->m_data[ksource];
  }

  if (dir == m_dir)
  {
    for (kdest=0; kdest<Kdest.m_K; kdest++)
    {
      vMin = buf->m_data[0] + data[0 + kdest*Ksource.m_K];
      for (ksource=1; ksource<Ksource.m_K; ksource++)
      {
        if (vMin > buf->m_data[ksource] + data[ksource + kdest*Ksource.m_K])
        {
          vMin = buf->m_data[ksource] + data[ksource + kdest*Ksource.m_K];
        }
      }
      m_message->m_data[kdest] = vMin;
    }
  }
  else
  {
    for (kdest=0; kdest<Kdest.m_K; kdest++)
    {
      vMin = buf->m_data[0] + data[kdest + 0*Kdest.m_K];
      for (ksource=1; ksource<Ksource.m_K; ksource++)
      {
        if (vMin > buf->m_data[ksource] + data[kdest + ksource*Kdest.m_K])
        {
          vMin = buf->m_data[ksource] + data[kdest + ksource*Kdest.m_K];	
        }
      }
      m_message->m_data[kdest] = vMin;
    }
  }

  vMin = m_message->m_data[0];
  for (kdest=1; kdest<Kdest.m_K; kdest++)
  {
    if (vMin > m_message->m_data[kdest])
    {
      vMin = m_message->m_data[kdest];
    }
  }

  for (kdest=0; kdest<Kdest.m_K; kdest++)
  {
    m_message->m_data[kdest] -= vMin;
  }

  delete [] data;
	return vMin;
}

inline void TypeTwostate::Edge::AddColumn(GlobalSize Kglobal, LocalSize Ksource, LocalSize Kdest, Label ksource, Vector* dest, int dir)
{
	assert(ksource>=0 && ksource<Ksource.m_K);

	int k;


  REAL* data = new REAL[Ksource.m_K * Kdest.m_K];
  for (int s=0; s <  Ksource.m_K ; s++)
  {
    for (int d=0 ; d < Kdest.m_K ; d++)
    {
      if (s == d)
        data[s+d*Ksource.m_K] = m_agreement;
      else
        data[s+d*Ksource.m_K] = m_disagreement;
    }
  }

  
		if (dir == m_dir)
		{
			for (k=0; k<Kdest.m_K; k++)
			{
				dest->m_data[k] += data[ksource + k*Ksource.m_K];
			}
		}
		else
		{
			for (k=0; k<Kdest.m_K; k++)
			{
				dest->m_data[k] += data[k + ksource*Kdest.m_K];
			}
		}
  
  delete [] data;
  
}

//////////////////////////////////////////////////////////////////////////////////

#endif
