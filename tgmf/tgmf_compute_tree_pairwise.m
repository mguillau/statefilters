function [GM,info] = gmf_compute_tree_pairwise(GM,pw_fun,pw_fun_args)

tree = GM.tree;
npairwise = 0;
unary_sizes = cellsizes(GM.UE);
unary_sizes = unary_sizes(:,2);
state_sizes = cellsizes(GM.filter.NI);
state_sizes = state_sizes(:,2);
empty_energy = cellisempty(GM.PE); %any(cellsizes(GM.PE),2);
total_time = tic();

for i=1:length(tree.parent),
    j = tree.parent(i);
    pi_idx = tree.parent_edge_index(i);
    if j>0 && empty_energy(pi_idx), % no pairwise term has been computed
        pi_order = tree.parent_edge_order(i);
        if pi_order, % compute i vs parent(i)
            E = Inf( unary_sizes(i), unary_sizes(j) );
            E( GM.filter.NI{i}, GM.filter.NI{j} ) = feval( pw_fun, i, GM.filter.NI{i}, j, GM.filter.NI{j}, pw_fun_args{:} );
            GM.PE{pi_idx} = E;
        else % compute parent(i) vs i
            E = Inf( unary_sizes(j), unary_sizes(i) );
            E( GM.filter.NI{j}, GM.filter.NI{i} ) = feval( pw_fun, j, GM.filter.NI{j}, i, GM.filter.NI{i}, pw_fun_args{:} );
            GM.PE{pi_idx} = E;
        end
        npairwise = npairwise + state_sizes(i)*state_sizes(j);
    end
end

info.time = toc(total_time);
info.new_pairwise = npairwise; 

