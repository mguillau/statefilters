function E = pw_dummy(i,K,j,L,GM)
% Read out the pairwise energy of states (k,l)\in KxL of edge (i,j) 

[edge,order] = gm_find_edge(GM,i,j);
if order,
    E = GM.PE{edge}(K,L);
else
    E = GM.PE{edge}(L,K);
end

