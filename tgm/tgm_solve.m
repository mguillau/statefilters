function [energy,solution,energy_lb] = tgm_solve(GM,varargin)

read_varargin;
defaultstr method 'bp';

if strcmp(method,'bp'),
  [energy,solution] = tgm_solve_bp(GM);
  energy_lb = energy; % bp is exact
else
  % use the tree to generate the correct PI matrix for use in TRW-S
  n = length(GM.tree.parent);
  PI = zeros(3,n,'uint32');
  for i=1:n,
      j = GM.tree.parent(i);
      f = GM.tree.parent_edge_order(i);
      if f,
          PI(1,i) = i;
          PI(2,i) = j;
      else
          PI(1,i) = j;
          PI(2,i) = i;        
      end
      PI(3,i) = GM.tree.parent_edge_index(i);
  end
  PI(:,GM.tree.root) = [];
  GM.PI = PI;
  GM = rmfield(GM,'tree');
  [energy,solution,energy_lb] = gm_solve(GM, varargin{:});
end

