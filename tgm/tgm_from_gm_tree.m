function GM = tgm_from_gm_tree(GM,tree)
% Add given tree structure on top of GM.

n = length(tree.parent);
tree.parent_edge_index = zeros(1,n);
tree.parent_edge_order = true(1,n);
for i=1:length(tree.parent),
    j=tree.parent(i);
    if j>0,
        [loc,order] = gm_find_edge(GM,i,j);
        if size(GM.PI,2)==3,
            tree.parent_edge_index(i) = GM.PI(3,loc);
        else
            tree.parent_edge_index(i) = loc;
        end
        tree.parent_edge_order(i) = order;
    end
end
GM.tree = tree;

