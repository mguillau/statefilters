function [energy,solution,info] = gm_solve_expandshrink(GM,betaSelect,varargin)
% GM_SOLVE_EXPANDSHRINK Solve a GM using alpha-expand/beta-shrink and variants (alpha-exp, alpha-beta swap, ...)
% 
% Based on Mark Schmidt's UGMep_Decode_ExpandShrink
%

global nonsubmodular_count move_count; % keep track of number of non-submodular terms and edges considered
nonsubmodular_count = 0; % init with 0
move_count = 0; % init with 0

assert_gm(GM);
default betaSelect 0;
read_varargin
default solution cellfun(@argmin,GM.UE);
default verbose true;

nNodes = length(GM.UE);
maxState = gm_maxstates(GM);
nEdges = size(GM.PI,2);

G = graph_from_gm(GM);
GM.binaryEdgeStruct = UGM_makeEdgeStruct(G+G',2); % pre-compute binary edge struct to be used in graphcuts

energy = gm_energy_of_configuration(GM,solution);
if verbose, switch betaSelect,
    case 0, mname = 'alpha-expansion';
    case 1, mname = 'alpha-expand/beta-shrink with random beta';
    case 2, mname = 'alpha-expand/beta-shrink, iterate beta';
    case 3, mname = 'alpha-expand/beta-shrink, iterate alpha';
    case 4, mname = 'alpha-expand/beta-shrink, beta=alpha-1';
    case 5, mname = 'alpha-expand/beta-shrink, beta=alpha+1';
    otherwise error('Invalid betaSelect %d',betaSelect); end
    pg_message('Using %s',mname);
    pg_message('Initial energy = %f',energy);
end
if verbose, pg=pg_start('Moves','iter %d (state %d/%d), energy %f, nonsubmodular %02.1f%%'); end

% Do moves until convergence
iter=0;
while 1
    iter=iter+1;
    solution_old = solution;
    S = randperm(maxState);
    if betaSelect==2 || betaSelect==3,
        T = randperm(maxState);
    end
    
    for u1 = 1:maxState
        s1 = S(u1);
        if verbose, pg.update(iter,u1,maxState,energy,100*nonsubmodular_count/move_count); end
        switch betaSelect
            case 0 % Basic alpha-expansion
                solution = applyMove(solution,s1,s1,GM);
            case 1 % Randomized selection of beta
                ind = [1:s1-1 s1+1:maxState];
                solution = applyMove(solution,s1,ind(ceil(rand*(maxState-1))),GM);
            case 2 % Iterate over choices of beta
                for u2 = 1:maxState
                    s2 = T(u2);
                    solution = applyMove(solution,s1,s2,GM);
                end
            case 3 % Iterate over choices of alpha
                for u2 = 1:maxState
                    s2 = T(u2);
                    solution = applyMove(solution,s2,s1,GM);
                end
            case 4 % Set beta = alpha-1
                solution = applyMove(solution,s1,max(s1-1,1),GM);
            case 5 % Set beta = alpha+1
                solution = applyMove(solution,s1,min(s1+1,maxState),GM);
        end
    end
    
    energy_old = energy;
    energy = gm_energy_of_configuration(GM,solution);
%    if verbose, pg_message('Iteration %d,\tEnergy = %f,\tChanges = %d',iter,energy,sum(solution_old~=solution)); end
    if all(solution==solution_old)
        if verbose, pg.done('States have converged, stopping'); end
        break;
    end
    if energy-energy_old == 0
        if verbose, pg.done('Energy has converged, stopping'); end
        break;
    end
end
if verbose, pg_message('Final energy = %f, Non-submodular energies = %02.1f%%',energy,100*nonsubmodular_count/move_count); end
info.total_moves = iter;
info.nonsubmodular_moves = nonsubmodular_count;

end

function y = applyMove(y,s1,s2,GM) %(nodeEnergy,edgeEnergy,edgeWeights,edgeStruct)
% TODO: deal with the case where s1 or s2 is not a valid state for this node
global nonsubmodular_count move_count;

nNodes = length(GM.UE);
maxState = gm_maxstates(GM);
nEdges = size(GM.PI,2);

modifiedNE = zeros(nNodes,2);
modifiedEE = zeros(2,2,nEdges);

for n = 1:nNodes
    nodeEnergy = GM.UE{n};
    if y(n) == s1
        modifiedNE(n,:) = [nodeEnergy(s1) nodeEnergy(s2) ]; % Keep alpha or shrink with beta
    else
        modifiedNE(n,:) = [nodeEnergy(s1) nodeEnergy(y(n))]; % Keep current state or switch to alpha
    end
end
if size(GM.PI,1)==2,
    I = uint32(1:nEdges);
else
    I = GM.PI(3,:);
end
for e = 1:nEdges
    n1 = GM.PI(1,e);
    n2 = GM.PI(2,e);
    y1 = y(n1);
    y2 = y(n2);
    if y1 == s1
        y1 = s2;
    end
    if y2 == s1
        y2 = s2;
    end
    edgeEnergy = GM.PE{I(e)};
    modifiedEE(:,:,e) = [ edgeEnergy(s1,s1) edgeEnergy(s1,y2)
                          edgeEnergy(y1,s1) edgeEnergy(y1,y2) ];

    % Check sub-modularity.
    move_count = move_count+1;
    if modifiedEE(1,1,e) + modifiedEE(2,2,e) > modifiedEE(1,2,e) +  modifiedEE(2,1,e),
        nonsubmodular_count = nonsubmodular_count+1;
    end
    % Change energy to make sub-modular (does nothing if edges are metric)
    if y(n1) == s1 && y(n2) == s1
        % (alpha,alpha) edge, decrease energy of staying at (alpha,alpha)
        modifiedEE(1,1,e) = min(modifiedEE(1,1,e),modifiedEE(1,2,e)+modifiedEE(2,1,e)-modifiedEE(2,2,e));
    elseif y(n1) == s1
        % (alpha,~alpha) edge, increase energy of changing to (~alpha,alpha)
        modifiedEE(2,1,e) = max(modifiedEE(2,1,e),modifiedEE(1,1,e)+modifiedEE(2,2,e)-modifiedEE(1,2,e));
    elseif y(n2) == s1
        % (~alpha,alpha) edge, increase energy of changing to (alpha,~alpha)
        modifiedEE(1,2,e) = max(modifiedEE(1,2,e),modifiedEE(1,1,e)+modifiedEE(2,2,e)-modifiedEE(2,1,e));
    else
        % (~alpha,~alpha) edge, decrease of energy of staying at (~alpha,~alpha)
        modifiedEE(2,2,e) = min(modifiedEE(2,2,e),modifiedEE(1,2,e)+modifiedEE(2,1,e)-modifiedEE(1,1,e));
    end
end
ytmp = UGMep_Decode_GraphCutFull(modifiedNE,modifiedEE,GM.binaryEdgeStruct)';
y(ytmp == 2 & y == s1) = s2;
y(ytmp == 1) = s1;
end
