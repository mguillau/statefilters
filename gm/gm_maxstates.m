function m = gm_maxstates(GM)
% GM_MAXSTATES Return the maximum number of states in a GM
%

assert_gm(GM);
s=cellsizes(GM.UE);
m=max(s(:,2));
