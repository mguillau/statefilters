function GMf = gm_filter_bestunaries(GM,k)

GMf = gm_init_filter(GM);
for i=1:length(GM.UE),
    unaries = GM.UE{i};
    [~,idx] = top(-unaries,k);
    GMf.filter.NI{i} = sort(idx);
end

