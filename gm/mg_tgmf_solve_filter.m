function [energy,solution,stats,mptime,energylb] = mg_tgmf_solve_filter(GM,model,varargin)

default NITER 20; % -> resue last filter as many times as needed


% in case of disjoint learning, find the best model
if ~model.joint,
    n0 = length(GM.UE);
    n1 = cellfun(@(x)  length(x.stat{1}.solution),model.models);
    modelIdx = mg_argmin(abs(n1./n0-1));
    thismodel = model.models{modelIdx};
    model = struct_merge(model,thismodel);
    % model = rmfield(model,'models');
end

GM = mg_tgm_init_filter(GM);
nt = sum(cellfun(@numel,GM.PE));
% keep track of filters with iterations: we can find where solutions where
% discarded
NIold = GM.filter.NI;
stats(1).npairwise = mg_tgmf_number_pairwise(GM);
stats(1).filter = GM.filter;
stats = repmat(stats,1,NITER);
G = mg_gm_undirected_graph_from_GM(GM);

% Pranks = zeros(NITER,102);
np = zeros(1,NITER);
tic;
iter = 1; done = false;
while ~done,
    NI = GM.filter.NI;
    
    % Extract features
    if iter==1 && model.withUnary, % first step: nothing filtered yet
        X = mg_tgmf_unary_features(GM);
        if model.mmAcc,
            ACCMM = GM.UE; 
            ACCNI = GM.filter.NI;
            if model.splitMM,
                ACCMMu = GM.UE;
                ACCMMp = cell(1,lenth(GM));
                for i=1:length(GM),
                    ACCMMp = zeros(1,length(GM.UE{i}));
                end
            end
        end
        np(iter) = 0;
    else
        parent = mg_graph_random_spanning_tree(G);
        tree = mg_tgm_tree_from_parent(parent);
        GM = mg_tgm_append_tree(GM,tree);
        if model.mmSplit,
            [MM,MMu,MMp,np(iter)] = mg_tgmf_maxmarginalssplit_bp(GM);
        else
            [MM,np(iter)] = mg_tgmf_maxmarginals_bp(GM); %
        end
        if iter>1 && model.mmAcc,
            MM = mg_tgmf_sum_incompatible_maxmarginals(MM,NI,ACCMM,ACCNI); %mg_tgmf_maxmarginals_bp(GM);
            if model.splitMM,
                MMu = mg_tgmf_sum_incompatible_maxmarginals(MMu,NI,ACCMMu,ACCNI); %mg_tgmf_maxmarginals_bp(GM);
                MMp = mg_tgmf_sum_incompatible_maxmarginals(MMp,NI,ACCMMp,ACCNI); %mg_tgmf_maxmarginals_bp(GM);
            end
        end
        if model.mmAcc,
            ACCMM = MM;
            ACCNI = NI;
            if model.splitMM,
                ACCMMu = MMu;
                ACCMMp = MMp;
            end
        end
        if model.mmSplit
            X = mg_tgm_maxmarginalssplit_features(GM,MM,MMu,MMp);
        else
            X = mg_tgm_maxmarginals_features(GM,MM);
        end
    end
    stats(iter).nptree=np(iter);
    
    % Apply filter
    if iter>model.niter,
        k = model.niter;
    else
        k = iter;
    end
    
%     P = mg_tgmf_filter_probs(X,model.filter{k});
%     ranks = cellfun(@rankof,mg_cellmap(@(x) 1-x,P),num2cell(solution),NI); % find rank in the subset of kept states for prob
%     Pranks(iter,:) = histc(ranks,-1:100);
%    fprintf('# total pw = %d\n# used pw in tree = %d\n',nt,np(iter));
    GM = mg_tgmf_apply_filter(GM,X,model.filter{k});
    % compute number of remaining pairwise terms
    npw = mg_tgmf_number_pairwise(GM);
    %npw = prod(cellfun(@numel,GM.filter.NI));
%    fprintf('# remaining pw = %d\n',npw);
    converged = all(cellfun(@(x,y) isempty(setdiff(x,y)),NI,GM.filter.NI)); % GM.filter.NI \subset NI
    if iter==NITER || npw<sum(np) || (converged && iter>=model.niter),
        done = true;
        toto=tic;
        [energy,solution,energylb] = mg_tgmf_solve_trws(GM);
        mptime=toc(toto);
%        fprintf('trws energy of filtered GM = %f\n',energy);
    end
    iter = iter+1;
    stats(iter).npairwise = npw;
    stats(iter).filter = GM.filter;
    stats(iter).nptree = npw;
end
stats = stats(1:iter);
