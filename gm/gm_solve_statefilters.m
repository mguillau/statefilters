function [energy,solution,info] = gm_solve_filter(GM,filtermodel,pw_fun,pw_fun_args,varargin)

read_varargin;
default verbose true;

G = graph_from_gm(GM); % get the graph of the GM
GM = gm_init_filter(GM); % add filtering info
EG = egraph_from_gm(GM); % efficient indexing of edges
filter_args = {G,EG,pw_fun,pw_fun_args};

not_converged = true;
iter = 0;

while not_converged

    iter = iter+1;
    if verbose, fprintf('StateFilter iteration %d\n',iter); end

    old_states = GM.filter.NI;

    % apply filter
    filter = filtermodel{min(iter,length(filtermodel))};
    [GM,info{iter}] = gmf_apply_filter(filter,GM,filter_args{:});

    not_converged = iter<length(filtermodel) && any( ~cellfun( @isequaln, old_states, GM.filter.NI ) ) ;

end
if verbose, fprintf('StateFilter filtering done. Computing remaining pairwise terms\n'); end
total_filter_pw = sum(cellfun(@(x) x.new_pairwise,info));

final = iter+1;

% no more filtering: compute remaining pairwise
[GM,info{final}] = gmf_compute_remaining_pairwise(GM,pw_fun,pw_fun_args);
info{final}.total_filter_pw = total_filter_pw;
info{final}.total_pw = sum(cellfun(@(x) x.new_pairwise,info));
info{final}.full_pw = gm_number_pairwise(GM);
info{final}.fraction_pw = info{final}.total_pw/info{final}.full_pw;

if true,
fprintf('Computed %d pairwise for filtering and %d to complete GM:\n Total %d vs %d for the full GM (%2.5f%%)\n',...
   info{final}.total_filter_pw,  info{final}.new_pairwise,  info{final}.total_pw,  info{final}.full_pw,  100*info{final}.fraction_pw);
end
% and solve
[energy,solution,info2] = gmf_solve(GM,'verbose',false);
info{final} = struct_merge(info{final},info2);

