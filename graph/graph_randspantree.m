function tree = graph_randspanparent(G)
% Create a random spanning tree from an undirected graph G
%

%read_varargin;
%default verbose false;
%DB=verbose;
%if DB, tt=tic(); end

N = size(G,1); % number of nodes
isfull = (2*nnz(G)==N*(N-1));

if isfull,
% super-fast mex code
parent = graph_randspantree_mex(N);

else

% random walk the GM to get a spanning parent
V = false(1,N); % visited nodes
i = randi(N,1); % random starting node = root
V(i) = true;
iter = 0;
parent = zeros(N,1);

while nnz(~V),
    iter = iter+1;
    if DB, fprintf('Node: %d,\t(un)visited: %d/%d,\t',i,nnz(V),nnz(~V)); end
    pi   = find(G(i,:)' | G(:,i));
    n    = length(pi);

    
    if n>0,
        k = randi(n,1);  % randomly pick an edge;
        j = pi(k); % corresponding neighbor
        if DB, fprintf('following edge %d -> %d\n',i,j); end
        if ~V(j), % the neighbour is unvisited            
            parent(j) = i;
            if DB, fprintf('New parent edge %d -> %d\n',j,i); end
        end
        i = j;
    else
        assert(false);
        uv = find(~V);
        i = uv(randi(length(uv),1)); % randomly pick an unvisited node
        if DB, fprintf('Random restart %d\n',i); end
    end
    
    V(i) = true;
end

if DB,fprintf('random spanning parent found in %d walk steps (%fs)\n',iter,toc(tt)); end

end

tree = tree_from_parent(parent);
