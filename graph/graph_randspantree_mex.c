#include <mex.h>
#include <matrix.h>
#include <math.h>
#include <inttypes.h>

#define DB 0

uint32_t randSample(uint32_t n) /* sample between 0...n-1 */
{
    double scale = ((double) n) / ((double) RAND_MAX);
    return (uint32_t) floor(rand() * scale);
}

/*
bool notall(bool * vec, uint32_t l) {
    while (l--)
        if (!vec[l]) return 1;
    return 0;
}
*/

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    /* Check number and type of inputs/outputs */
    if (nrhs < 1 || nrhs > 1)
        mexErrMsgTxt("Only one argument.");
    if (nlhs > 1)
        mexErrMsgTxt("Only one output.");

    /* Read input */
    uint32_t n_nodes = (uint32_t) mxGetScalar(prhs[0]);
    if (DB) mexPrintf("Sampling a tree for a fully-connected graph with %d nodes\n",n_nodes);

    /* Create output */
    plhs[0] = mxCreateNumericMatrix(n_nodes, 1, mxDOUBLE_CLASS, mxREAL); /* output vector */
    double * parent = mxGetPr(plhs[0]); /* access to output vector */

    bool * visited = calloc(n_nodes,sizeof(bool)); /* temporary data */   
    uint32_t root = randSample(n_nodes);
    visited[root] = 1;
    if (DB) mexPrintf("Using root %d\n",root+1);
    
    uint32_t i = root, j; /* start from root */
    int iter = 0;
    int total_visited = 1;
    
    while (total_visited<n_nodes) { /* notall(visited,n_nodes) &&  */
        if (DB) mexPrintf("At node %d\n",i+1);
        iter++;
        j = randSample(n_nodes-1); /* sample a neighbour */
        if (j>=i) j++; /* real corresponding neighbour */
        if (DB) mexPrintf("Sampled %d\n",j+1);
        if (!visited[j]) {
            parent[j]=(double) (i+1); /* store it in 1-based indexing for matlab */
            total_visited++;
        }
        if (DB) {
            if (visited[j]) mexPrintf("%d already visited\n",j+1);        
            else mexPrintf("%d not visited: adding edge %d-->%d to tree\n",j+1,i+1,j+1); 
        }
        i=j;
        visited[i]=1;
        if (DB) mexPrintf("Total visited %d\n",total_visited);
    }

    free(visited);

    return;
}


