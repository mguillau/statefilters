function tree = tree_toposort(tree)
% Add topological information to a tree

DB=0;

% Sort (counting-sort: linear time) nodes wrt their distance to origin
n = numel(tree.parent);
parent = tree.parent;
parent(tree.root) = tree.root;

idx = zeros(1,n);
u = 1;
idx(u) = tree.root;
V = false(1,n); V(tree.root) = true;
L = false(1,n); L(tree.root) = true;
while ~all(V),
    if DB, fprintf('Visited=%d,Last=%d\n',nnz(V),nnz(L)); end
    L     = L(parent); % new nodes
    L(tree.root) = false;
    idx(u+1:u+nnz(L)) = find(L);
    u = u+nnz(L);
    V     = V|L;  % new nodes are now visited    
end

tree.root2leaves = idx;
tree.leaves2root = fliplr(idx);
